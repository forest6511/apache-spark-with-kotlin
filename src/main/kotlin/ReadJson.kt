import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.jetbrains.kotlinx.spark.api.withSpark

fun main() {
    val path = "src/main/resources/customers.json"
    withSpark {
        val custDF: Dataset<Row>? = spark.read().json(path)
        custDF?.printSchema()
        println(custDF?.count())
    }
}