import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.jetbrains.kotlinx.spark.api.withSpark

fun main() {
    val path = "src/main/resources/products.csv"
    withSpark {
        val prodDF: Dataset<Row> = spark.read().option("header",false).csv(path)
        prodDF.printSchema()
        println(prodDF.count())
        prodDF.show()
    }
}