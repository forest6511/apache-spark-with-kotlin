import org.jetbrains.kotlinx.spark.api.*


fun main() {
    val logFile = "src/main/resources/moviedata.json"
    withSpark {
        spark.read().textFile(logFile).withCached {
            val numAs = filter { it.contains("a") }.count()
            val numBs = filter { it.contains("b") }.count()
            println("Lines with a: $numAs, lines with b: $numBs")
        }
    }
}