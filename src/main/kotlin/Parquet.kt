import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.jetbrains.kotlinx.spark.api.withSpark

fun main(){
    val path = "src/main/resources/episodes.parquet"
    withSpark {
        val df: Dataset<Row> = spark.read().parquet(path)
        df.printSchema()
        df.filter{ it.getInt(2) == 11}
            .show(5)
    }
}
